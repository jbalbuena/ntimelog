import java.io.*;

class Task4 {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		String[][] activityDetails = {{"Outbreak BGC", "Outbreak Laguna"}, {"1730", "1030"}, {"0000", "1500"}};
		int userSelection;
		
		System.out.println("Activity Viewer v1.0" + "\nPlease note that the time is in the 24-hr format.");
		System.out.println("\n" + "[1] Outbreak Nuvali" + "\n[2] Outbreak Laguna" + "\n[3] All Activities");
		System.out.print("\nSelect Activity to view: ");
		userSelection = Integer.parseInt(br.readLine());
		
		System.out.println("\nEvent Name:" + "\tStart:" + "\tEnd:");
		switch(userSelection) {
			case 1: {
				for(String[] i : activityDetails) {
					System.out.print(i[userSelection - 1] + "\t");
				}
				break;
			}
			case 2: {
				for(String[] i : activityDetails) {
					System.out.print(i[userSelection - 1] + "\t");
				}
				break;
			}
			default: {
				for(int x = 0; x < 2; x++) {
					for(String[] i : activityDetails) {
						System.out.print(i[x] + "\t");
					} 
					System.out.print("\n");
				}
			}
		}
	}
}
