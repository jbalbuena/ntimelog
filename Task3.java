import java.io.*;

class Task3 {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		String activityName;
		String startTime;
		String endTime;
		
		System.out.println("Activity Adder v1.0" + "\nPlease note that the time is in the 24-hr format.");
		System.out.print("\nEnter Activity Name: ");
		activityName = br.readLine();
		System.out.print("Enter Start time of activity: ");
		startTime = br.readLine();
		System.out.print("Enter End time of activity: ");
		endTime = br.readLine();
		
		System.out.print("\nActivity: " + activityName + " with Start time: " + startTime + " and End time: " + endTime + " successfully added.");
		
	}
}
