import java.io.*;

class Task1 {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		String userName;
		System.out.println("Log In Module v1.0");
		System.out.print("\nEnter User Name: ");
		userName = br.readLine();
		System.out.println("\nUser " + userName + " has logged in.");
	}
}
